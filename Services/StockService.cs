﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.Net;
using System.Net.Http;

using Newtonsoft.Json;

using StocksApp.Model;
using StocksApp.Model.StockModel;
using StocksApp.Model.TransactionModel;
using StocksApp.Model.UserModel;

namespace StocksApp.Services
{
    public interface IStockService
    {
        Task<StockModel> GET(string symbol);
        Task<IEnumerable<StockModel>> GETALL(List<string> symbols);
        Task<User> BUY(StockRequest stock, string token);
        Task<User> SELL(StockRequest stock, string token);
    }

    public class StockService : IStockService
    {
        private readonly IStockContext _context;
        private readonly IUserService _userService;

        public StockService(IStockContext context, IUserService userService)
        {
            _context = context;
            _userService = userService;
        }

        public async Task<StockModel> GET(string symbol)
        {
            var response = await _context.GetStocksBySymbols(new List<string> { symbol });
            return response.Count() == 0 ? null : response[0];
        }

        //Returns a response with multiple stocks.
        public async Task<IEnumerable<StockModel>> GETALL(List<string> symbols)
        {
            return await _context.GetStocksBySymbols(symbols);
        }

        public async Task<User> BUY(StockRequest stock, string token)
        {
            var user = await _userService.GetCurrentUserAsync(token);
            if (user == null)
            {
                throw new Exception("Token is invalid");
            }

            if (user.InvestmentPortfolio.cashAvailable < stock.Amount * stock.Price)
            {
                throw new FormatException("Not enough Cash!");
            }

            if (user?.InvestmentPortfolio?.holdings == null)
            {
                user.InvestmentPortfolio.holdings = new List<Model.InvestmentModel.StockInvested>();
            }
            bool foundStock = false;
            var history = await _context.GetStockHistory(stock.Symbol);
            foreach (Model.InvestmentModel.StockInvested _stock in user.InvestmentPortfolio.holdings)
            {
                if (_stock.symbol == stock.Symbol)
                {
                    _stock.priceAquired = (_stock.amount * _stock.priceAquired + stock.Amount * stock.Price) / (_stock.amount + stock.Amount);
                    _stock.amount = _stock.amount + stock.Amount;
                    _stock.totalValue = _stock.totalValue + stock.Amount * stock.Price;
                    _stock.currentPrice = stock.Price;
                    _stock.history = history;
                    user.InvestmentPortfolio.cashAvailable -= stock.Amount * stock.Price;
                    foundStock = true;
                    break;
                }
            }

            if (!foundStock)
            {
                user.InvestmentPortfolio.holdings.Add(new Model.InvestmentModel.StockInvested()
                {
                    name = stock.Name,
                    symbol = stock.Symbol,
                    amount = stock.Amount,
                    priceAquired = stock.Price,
                    totalValue = stock.Amount * stock.Price,
                    currentPrice = stock.Price,
                    percentageGainLoss = 0,
                    amountGainLoss = 0,
                    history = history,
                });
                user.InvestmentPortfolio.cashAvailable -= stock.Amount * stock.Price;
            }
            var transaction = new Transaction()
            {
                Type = TransactionType.buy,
                StockName = stock.Name,
                StockSymbol = stock.Symbol,
                Amount = stock.Amount,
                Price = stock.Price,
                TimeStamp = DateTime.Now,
            };
            if (user.Transactions == null)
            {
                user.Transactions = new List<Transaction>()
                {
                    transaction,
                };
            }
            else
            {
                user.Transactions.Add(transaction);
            }


            await _userService.UpdateUser(user);

            return user;

        }

        public async Task<User> SELL(StockRequest stock, string token)
        {
            var user = await _userService.GetCurrentUserAsync(token);
            if (user == null)
            {
                throw new Exception("Token is invalid");
            }

            if (user?.InvestmentPortfolio?.holdings == null)
            {
                user.InvestmentPortfolio.holdings = new List<Model.InvestmentModel.StockInvested>();
            }

            if (user?.InvestmentPortfolio?.holdings != null)
            {
                Model.InvestmentModel.StockInvested foundStock = null;
                foreach (Model.InvestmentModel.StockInvested _stock in user.InvestmentPortfolio.holdings)
                {
                    if (_stock.symbol == stock.Symbol)
                    {
                        if (_stock.amount < stock.Amount)
                        {
                            throw new FormatException("Can't sell more than you have!");
                        }

                        user.InvestmentPortfolio.cashAvailable += stock.Amount * stock.Price;

                        if (_stock.amount == stock.Amount)
                        {
                            foundStock = _stock;
                            break;
                        }

                        _stock.amount = _stock.amount - stock.Amount;
                        _stock.currentPrice = stock.Price;
                        _stock.totalValue = _stock.amount * _stock.currentPrice;
                        break;
                    }
                }

                if (foundStock != null)
                {
                    //delete it here
                    user.InvestmentPortfolio.holdings = user.InvestmentPortfolio.holdings.FindAll(e => e.symbol != foundStock.symbol);
                }
                var transaction = new Transaction()
                {
                    Type = TransactionType.sell,
                    StockName = stock.Name,
                    StockSymbol = stock.Symbol,
                    Amount = stock.Amount,
                    Price = stock.Price,
                    TimeStamp = DateTime.Now,
                };
                if (user.Transactions == null)
                {
                    user.Transactions = new List<Transaction>()
                {
                    transaction,
                };
                }
                else
                {
                    user.Transactions.Add(transaction);
                }

                await _userService.UpdateUser(user);

                return user;

            }
            else
            {
                throw new Exception("This never should be a case!");
            }
        }
    }
}
