﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Diagnostics;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using StocksApp.Helpers;
using Firebase.Database.Query;
using Microsoft.AspNetCore.Identity;

using StocksApp.Model.UserModel;
using System.Collections.Generic;
using StocksApp.Model.InvestmentModel;

namespace StocksApp.Services
{
    public interface IUserService
    {
        Task<User> Register(RegisterModel credentials);

        Task<string> GetQuestion(string email);
        Task<bool> PostQuestion(ForgotPassword model);
        Task<bool> ResetPassword(ForgotPassword model);

        Task<User> Authenticate(string email, string password);
        Task<User> GetCurrentUserAsync(string token);
        Task<User> UpdateUser(User user);
    }


    public class UserService: IUserService
    {
        private readonly AppSettings _appSettings;
        private readonly IUserContext _context;
        private readonly PasswordHasher<string> passwordHasher;
        private readonly JwtSecurityTokenHandler tokenHandler;
        
        public UserService(IOptions<AppSettings> appSettings, IUserContext context)
        {
            _appSettings = appSettings.Value;
            _context = context;
            passwordHasher = new PasswordHasher<string>();
            tokenHandler = new JwtSecurityTokenHandler();
        }

        private string GenerateToken(string id, string email)
        {
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] {
                    new Claim(ClaimTypes.NameIdentifier, id),
                    new Claim(ClaimTypes.Email, email)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public async Task<User> Register(RegisterModel credentials)
        {
            var foundUser = await _context.GetUserByEmail(credentials.Email);

            if (foundUser != null)
            {
                return null;
            }

            var user = new User
            {
                Id = Guid.NewGuid().ToString(),
                FirstName = credentials.FirstName,
                LastName = credentials.LastName,
                Email = credentials.Email,
                Question = credentials.Question,
                Answer = credentials.Answer,
                InvestmentPortfolio = new InvestmentPortfolio()
                {
                    cashAvailable = 200,
                    totalValue = 200,
                    totalGainLoss = 0,
                    percentGainLoss = 0,
                    holdings = new List<StockInvested>(),
                },
                Transactions = new List<Model.TransactionModel.Transaction>(),
            };
            user.Password = passwordHasher.HashPassword(user.Id, credentials.Password);
            user.Token = GenerateToken(user.Id, user.Email);

            await _context.PutUser(user);

            return user;
        }

        public async Task<User> Authenticate(string email, string password)
        {
            var foundUser = await _context.GetUserByEmail(email);
            Debug.WriteLine(foundUser);
            if (foundUser == null)
            {
                return foundUser;
            }

            if (passwordHasher.VerifyHashedPassword(foundUser.Id, foundUser.Password, password).Equals(PasswordVerificationResult.Failed))
            {
                return null;
            }

            foundUser.Token = GenerateToken(foundUser.Id, foundUser.Email);
            return foundUser;

        }

        public async Task<User> GetCurrentUserAsync(string token)
        {
            token = token.Split(" ")[1];
            var tokenS = tokenHandler.ReadToken(token) as JwtSecurityToken;

            var id = tokenS.Claims.FirstOrDefault(claim => claim.Type == "nameid").Value;

            return await _context.GetUserById(id);
        }

        public async Task<User> UpdateUser(User user)
        {
            await _context.PutUser(user);
            return user;
        }

        public async Task<string> GetQuestion(string email)
        {
            return await _context.GetQuestion(email);
        }

        public async Task<bool> PostQuestion(ForgotPassword model)
        {
            return await _context.PostQuestion(model.Email, model.Answer);
        }

        public async Task<bool> ResetPassword(ForgotPassword model)
        {
            return await _context.ResetPassword(model.Email, model.Password, passwordHasher);
        }
    }
}
