## API Documentation:

API url: `http://ec2-3-81-202-234.compute-1.amazonaws.com`

### Auth Routes

#### Register:

`POST ~/api/auth/register`

data example:
```
{
	"firstName": "Huyen",
	"lastName": "Vu",
	"email": "huyen.vu@ttu.edu",
	"password": "password123"
}
```

#### Login
`POST ~/api/auth/login`

```
{
	"email": "huyen.vu@ttu.edu",
	"password": "password123"
}
```

#### Get Current User
`GET ~/api/auth`

#### Get List of stocks
`GET ~api/StockOptions`

header example:
`Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiIxODhlNWYyNS05ZjU2LTQ1ZTItOWY4OC04YjRhMTM4NWE0YTciLCJlbWFpbCI6Imh1eWVuLnZ1M0B0dHUuZWR1IiwibmJmIjoxNTgzNjAzNTc3LCJleHAiOjE1ODQyMDgzNzcsImlhdCI6MTU4MzYwMzU3N30.rKihmMjatuPxXoYV7Ws3FLYSww8YCvyXVUD0dcaqtKY`


#### Schema:

User:
 - Id
 - FirstName
 - LastName
 - Email
 - Password
 - Token
 - CashAvailable
 - OwningStocks
 - Transactions

Stock:
 <From API>

Transactions
 - Id
 - Type
 - TimeStamp
 - StockName
 - StockSymbol
 - Amount
 - Price